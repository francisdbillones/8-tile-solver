import copy
import time

class State:
	def __init__(self, state, distance):
		self.state = state
		self.distance = distance

goal_state = State([
			['1', '2', '3'],
			['4', '5', '6'],
			['7', '8', '_']
		], 0)

def main():
	tiles = getTiles()

	initial_state = State(tiles, 0)
	initial_state.distance = getDistance(initial_state, goal_state)

	move_names = ["move up", "move down", "move left", "move right"]
	moves = solve(initial_state)

	for move in moves:
		print(move_names[moves])

def solve(initial_state):
	moves = getMoves(initial_state)
	return moves

def getMoves(initial_state):
	moves = []

	# 0 = move up
	# 1 = move down
	# 2 = move left
	# 3 = move right

	current_state = copy.deepcopy(initial_state)
	print("initial_state: ", initial_state.state)
	seen = []
	while current_state.state != goal_state.state:
		current_indexes = getIndexes(current_state)
		
		hole_row_index = current_indexes['_'][0]
		hole_col_index = current_indexes['_'][1]

		can_move_up = hole_row_index - 1 >= 0
		can_move_down = hole_row_index + 1 <= 2
		can_move_left = hole_col_index - 1 >= 0
		can_move_right = hole_col_index + 1 <= 2

		state1 = None
		state2 = None
		state3 = None
		state4 = None

		if can_move_up:
			state1 = move_up(current_state, hole_row_index, hole_col_index)

		if can_move_down:
			state2 = move_down(current_state, hole_row_index, hole_col_index)

		if can_move_left:
			state3 = move_left(current_state, hole_row_index, hole_col_index)

		if can_move_right:
			state4 = move_right(current_state, hole_row_index, hole_col_index)

		states = [state1, state2, state3, state4]
		seen += [state.state for state in states if state]
		best_state = [state for state in states if state][0]
		best_move = states.index(best_state)

		for move, state in enumerate(states):
			if state:
				print(move+1, " ", state.distance)
				if state.distance < best_state.distance and state.state not in seen:
					best_state = state
					best_move = move
		current_state = best_state

		move_names = ["move up", "move down", "move left", "move right"]
		pos_moves = [can_move_up, can_move_down, can_move_left, can_move_right]

		for i, move in enumerate(pos_moves):
			if move:
				print("I can ", move_names[i], end=", ")
			else:
				print("I can't ", move_names[i], end=", ")
		print()

		print("best_move is ", move_names[best_move])
		print("current_state is ", current_state.state)
		time.sleep(1)

		moves.append(best_move)

	return moves

def move_up(initial_state, hole_row_index, hole_col_index):
	temp_state = copy.deepcopy(initial_state)
	temp = initial_state.state[hole_row_index-1][hole_col_index]
	temp_state.state[hole_row_index-1][hole_col_index] = initial_state.state[hole_row_index][hole_col_index]
	temp_state.state[hole_row_index][hole_col_index] = temp

	manhattan_distance = getDistance(temp_state, goal_state)

	new_state = State(temp_state.state, manhattan_distance)

	return new_state

def move_down(initial_state, hole_row_index, hole_col_index):
	temp_state = copy.deepcopy(initial_state)
	temp = initial_state.state[hole_row_index+1][hole_col_index]
	temp_state.state[hole_row_index+1][hole_col_index] = initial_state.state[hole_row_index][hole_col_index]
	temp_state.state[hole_row_index][hole_col_index] = temp

	manhattan_distance = getDistance(temp_state, goal_state)

	new_state = State(temp_state.state, manhattan_distance)

	return new_state

def move_left(initial_state, hole_row_index, hole_col_index):
	temp_state = copy.deepcopy(initial_state)
	temp = initial_state.state[hole_row_index][hole_col_index-1]
	temp_state.state[hole_row_index][hole_col_index-1] = initial_state.state[hole_row_index][hole_col_index]
	temp_state.state[hole_row_index][hole_col_index] = temp

	manhattan_distance = getDistance(temp_state, goal_state)

	new_state = State(temp_state.state, manhattan_distance)

	return new_state

def move_right(initial_state, hole_row_index, hole_col_index):
	temp_state = copy.deepcopy(initial_state)
	temp = initial_state.state[hole_row_index][hole_col_index+1]
	temp_state.state[hole_row_index][hole_col_index+1] = initial_state.state[hole_row_index][hole_col_index]
	temp_state.state[hole_row_index][hole_col_index] = temp

	manhattan_distance = getDistance(temp_state, goal_state)

	new_state = State(temp_state.state, manhattan_distance)

	return new_state

def getIndexes(state):
	indexes = {}
	for i, ith in enumerate(state.state):
		for j, jth in enumerate(ith):
			indexes[jth] = [i, j]
	return indexes

def getDistance(new_state, goal_state):
	goal_indexes = getIndexes(goal_state)
	new_indexes = getIndexes(new_state)

	distance = 0
	for i in goal_indexes:
		if i == '_':
			continue
		goal_index = goal_indexes[i]
		new_index = new_indexes[i]

		if goal_index != new_index:
			row_distance = abs(goal_index[0]-new_index[0])
			col_distance = abs(goal_index[1]-new_index[1])

			distance += row_distance + col_distance

	return distance

def getTiles():
	tiles = []
	row_count = 1
	seen = []
	while True:
		row = input("Row " + str(row_count) + ": ").split()
		for element in row:
			if not element.isnumeric() and element != '_':
				print("Must contain numeric values and '_' to denote the blank tile.")
				break
			if element in seen:
				print("Has duplicates.")
				getTiles()
			if element == '9':
				print("Must not contain 9.")
				getTiles()
			seen.append(element)
		tiles.append(row)
		row_count += 1
		if row_count > 3:
			break
	return tiles

def hasDuplicates(tiles):
	seen = []
	for row in tiles:
		for element in row:
			if element in seen:
				return True
			seen.append(element)
	return False

main()

